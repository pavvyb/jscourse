'use strict';

function makeFib() {
    let first = -1
    let result = 1
    let newFib
    function nextFibNumber() {
        if (first<1){
            newFib=1
            first++
        } else {
            newFib=result+first
            first=result
            result=newFib
        }
        console.log(newFib)
        return newFib
    }
    return nextFibNumber
}
const fibonacci = makeFib();

fibonacci();
fibonacci();
fibonacci();
fibonacci();
fibonacci();
